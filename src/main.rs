use chrono::offset::Local;
use chrono::prelude::*;
use clap::App;
use clap::Arg;
use clap::ArgMatches;

fn main() {
    let matches = App::new("canticle")
        .version("0.0.5")
        .about("Prints daily collects from the Book of Common Prayer, 2019.\nUses current day and time by default.")
        .author("Chip Altman")
        .arg(
            Arg::with_name("day")
                .short("d")
                .long("day")
                .takes_value(true)
                .help("Provide <day> argument, in format:\nsun, mon, tue, wed, thu, fri, sat"),
        )
        .arg(
            Arg::with_name("morning")
                .short("m")
                .long("morning")
                .takes_value(false)
                .help("Prints collect for morning"),
        )
        .arg(
            Arg::with_name("evening")
                .short("e")
                .long("evening")
                .takes_value(false)
                .help("Prints collect for evening"),
        )
        .get_matches();

    parse_args(matches);
}

fn parse_args(matches: ArgMatches) {
    if matches.is_present("morning") {
        if matches.is_present("day") {
            match matches
                .value_of("day")
                .expect("a day, or maybe some nonsense I guess.")
            {
                "sun" => manual_am(Weekday::Sun),
                "mon" => manual_am(Weekday::Mon),
                "tue" => manual_am(Weekday::Tue),
                "wed" => manual_am(Weekday::Wed),
                "thu" => manual_am(Weekday::Thu),
                "fri" => manual_am(Weekday::Fri),
                "sat" => manual_am(Weekday::Sat),
                _ => print!("sun, mon, tue, wed, thu, fri, sat"),
            }
        } else {
            auto_am();
        }
    }

    if matches.is_present("evening") {
        if matches.is_present("day") {
            match matches
                .value_of("day")
                .expect("a day, or maybe some nonsense I guess.")
            {
                "sun" => manual_pm(Weekday::Sun),
                "mon" => manual_pm(Weekday::Mon),
                "tue" => manual_pm(Weekday::Tue),
                "wed" => manual_pm(Weekday::Wed),
                "thu" => manual_pm(Weekday::Thu),
                "fri" => manual_pm(Weekday::Fri),
                "sat" => manual_pm(Weekday::Sat),
                _ => print!("sun, mon, tue, wed, thu, fri, sat"),
            }
        } else {
            auto_pm();
        }
    }

    if matches.is_present("day") & !matches.is_present("morning") & !matches.is_present("evening") {
        match matches
            .value_of("day")
            .expect("a day, or maybe some nonsense I guess.")
        {
            "sun" => {
                manual_am(Weekday::Sun);
                manual_pm(Weekday::Sun);
            }
            "mon" => {
                manual_am(Weekday::Mon);
                manual_pm(Weekday::Mon);
            }
            "tue" => {
                manual_am(Weekday::Tue);
                manual_pm(Weekday::Tue);
            }
            "wed" => {
                manual_am(Weekday::Wed);
                manual_pm(Weekday::Wed);
            }
            "thu" => {
                manual_am(Weekday::Thu);
                manual_pm(Weekday::Thu);
            }
            "fri" => {
                manual_am(Weekday::Fri);
                manual_pm(Weekday::Fri);
            }
            "sat" => {
                manual_am(Weekday::Sat);
                manual_pm(Weekday::Sat);
            }
            _ => print!("sun, mon, tue, wed, thu, fri, sat"),
        }
    }

    if !matches.is_present("morning") & !matches.is_present("evening") & !matches.is_present("day")
    {
        default();
    }
}

fn default() {
    let time = Local::now().hour12();
    match time {
        (true, _) => auto_pm(),
        (false, _) => auto_am(),
    };
}

fn auto_am() {
    let day = Local::now().weekday();
    let _sun_am = include_str!("collects/00_collect_sun_am.txt");
    let _mon_am = include_str!("collects/01_collect_mon_am.txt");
    let _tue_am = include_str!("collects/02_collect_tue_am.txt");
    let _wed_am = include_str!("collects/03_collect_wed_am.txt");
    let _thu_am = include_str!("collects/04_collect_thu_am.txt");
    let _fri_am = include_str!("collects/05_collect_fri_am.txt");
    let _sat_am = include_str!("collects/06_collect_sat_am.txt");

    match day {
        Weekday::Sun => print!("{}", _sun_am),
        Weekday::Mon => print!("{}", _mon_am),
        Weekday::Tue => print!("{}", _tue_am),
        Weekday::Wed => print!("{}", _wed_am),
        Weekday::Thu => print!("{}", _thu_am),
        Weekday::Fri => print!("{}", _fri_am),
        Weekday::Sat => print!("{}", _sat_am),
    }
}

fn auto_pm() {
    let day = Local::now().weekday();
    let _sun_pm = include_str!("collects/00_collect_sun_pm.txt");
    let _mon_pm = include_str!("collects/01_collect_mon_pm.txt");
    let _tue_pm = include_str!("collects/02_collect_tue_pm.txt");
    let _wed_pm = include_str!("collects/03_collect_wed_pm.txt");
    let _thu_pm = include_str!("collects/04_collect_thu_pm.txt");
    let _fri_pm = include_str!("collects/05_collect_fri_pm.txt");
    let _sat_pm = include_str!("collects/06_collect_sat_pm.txt");

    match day {
        Weekday::Sun => print!("{}", _sun_pm),
        Weekday::Mon => print!("{}", _mon_pm),
        Weekday::Tue => print!("{}", _tue_pm),
        Weekday::Wed => print!("{}", _wed_pm),
        Weekday::Thu => print!("{}", _thu_pm),
        Weekday::Fri => print!("{}", _fri_pm),
        Weekday::Sat => print!("{}", _sat_pm),
    }
}

fn manual_am(day: Weekday) {
    let _sun_am = include_str!("collects/00_collect_sun_am.txt");
    let _mon_am = include_str!("collects/01_collect_mon_am.txt");
    let _tue_am = include_str!("collects/02_collect_tue_am.txt");
    let _wed_am = include_str!("collects/03_collect_wed_am.txt");
    let _thu_am = include_str!("collects/04_collect_thu_am.txt");
    let _fri_am = include_str!("collects/05_collect_fri_am.txt");
    let _sat_am = include_str!("collects/06_collect_sat_am.txt");

    match day {
        Weekday::Sun => print!("{}", _sun_am),
        Weekday::Mon => print!("{}", _mon_am),
        Weekday::Tue => print!("{}", _tue_am),
        Weekday::Wed => print!("{}", _wed_am),
        Weekday::Thu => print!("{}", _thu_am),
        Weekday::Fri => print!("{}", _fri_am),
        Weekday::Sat => print!("{}", _sat_am),
    }
}

fn manual_pm(day: Weekday) {
    let _sun_pm = include_str!("collects/00_collect_sun_pm.txt");
    let _mon_pm = include_str!("collects/01_collect_mon_pm.txt");
    let _tue_pm = include_str!("collects/02_collect_tue_pm.txt");
    let _wed_pm = include_str!("collects/03_collect_wed_pm.txt");
    let _thu_pm = include_str!("collects/04_collect_thu_pm.txt");
    let _fri_pm = include_str!("collects/05_collect_fri_pm.txt");
    let _sat_pm = include_str!("collects/06_collect_sat_pm.txt");

    match day {
        Weekday::Sun => print!("{}", _sun_pm),
        Weekday::Mon => print!("{}", _mon_pm),
        Weekday::Tue => print!("{}", _tue_pm),
        Weekday::Wed => print!("{}", _wed_pm),
        Weekday::Thu => print!("{}", _thu_pm),
        Weekday::Fri => print!("{}", _fri_pm),
        Weekday::Sat => print!("{}", _sat_pm),
    }
}
