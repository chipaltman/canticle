# Canticle
*An inaccurately named cli for printing collects from The Book of Common Prayer, 2019.*  

```dontyoudarestylethis
canticle 0.0.#
Chip Altman
Prints daily collects from the Book of Common Prayer, 2019.
Uses current day and time by default.

USAGE:
    canticle [FLAGS] [OPTIONS]

FLAGS:
    -e, --evening    Prints collect for evening
    -h, --help       Prints help information
    -m, --morning    Prints collect for morning
    -V, --version    Prints version information

OPTIONS:
    -d, --day <day>    Provide <day> argument, in format:
                       sun, mon, tue, wed, thu, fri, sat
```
## Example
```dontyoudarestylethis

% canticle -ed mon

A COLLECT FOR PEACE - Monday PM

O God, the source of all holy desires, all good counsels,
and all just works: Give to your servants that peace which
the world cannot give, that our hearts may be set to obey your
commandments, and that we, being defended from the fear of our
enemies, may pass our time in rest and quietness;
through the merits of Jesus Christ our Savior. Amen.
```
Text taken from the *The Book of Common Prayer (2019).*  
*Copyright © 2019 by the
<a href=http://bcp2019.anglicanchurch.net/>
Anglican Church in North America* </a>
